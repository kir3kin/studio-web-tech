<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('WP_CACHE', false); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '' ); //Added by WP-Cache Manager
define('DB_NAME', 'wp_studio');

/** Имя пользователя MySQL */
define('DB_USER', 'alex');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '1234');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bX<ax;d Q_[YPM~:}.R^tdk<ZSc:p.SjDBwO$9fU.EqP<Mf OL`ttOKK5^5`h$;:');
define('SECURE_AUTH_KEY',  '@#N|ei ?~k;X1ng#8P@oOZuOSV071k[gK9PM:oFF=<BS:~L0<xYG.mPO2^c|Z%:U');
define('LOGGED_IN_KEY',    '1A>=kH1|~9X9ih,_^?yXn|B[PC{YJsWYU{<%txQo1xbat7~FWd1<&5Wuz|H%5geR');
define('NONCE_KEY',        ':v#KTXxjvSRyEpRD4`^2w?^YLCeE5vLJ|I!f :j5oO%WD5/};bXK/kL39n~g{y9.');
define('AUTH_SALT',        'Z[hByDSmKZ:1<Rd?Y +b2VO(|OWpR0QO>SCmWE[{`rc0L6^]Y7IuctIz!X88+NCp');
define('SECURE_AUTH_SALT', ',Bd$)EW1.pY>FRB`]F3`[BHVQR.GY=2;j2*}Js9g/GvE/fNY8^BIOie~BEQO}%0q');
define('LOGGED_IN_SALT',   'E^8A:=Z~kfa*g3&{F+PiqVto7!@vmA3!-4irf,mI64NHV`X%!>/^:LyR]4rP9mnP');
define('NONCE_SALT',       '4D1oe9f&VfXlr@UL^[IJEwsH`J6qjmN+6Vzh9KGChHwoZPzQ&_FMf kEAX4+y8KL');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'st_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');