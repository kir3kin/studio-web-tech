<?php
/**
*Template name:Блог
*/
?>
<?php echo do_shortcode("[uptolike]"); ?>
<?php get_header(); ?>
<div class="bg_container-fluid">
	<div class="bg_container">
		<div class="bg_blogs">
<?php $cat_name = $GLOBALS["category_name"];
$posts = give_posts($cat_name);
foreach( $posts as $post ){ the_post(); ?>
			<div class="bg_blog-item">
				<?php //if (!get_the_post_thumbnail()) {если нет миниатюры ?>
					<img src="http://placehold.it/800x200"></img>
				<?php //} ?>
				<?php //echo get_the_post_thumbnail(); ?>
				<?php the_content(""); ?>
				<div class="more-link-wr">
					<a class="more-link" href="<?php echo home_url()."/".get_post()->post_name; ?>">Подробнее...</a>
				</div>
			</div>
<?php } ?>
		</div>
<?php get_sidebar(); ?>
	<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
</div>
<?php get_footer(); ?>