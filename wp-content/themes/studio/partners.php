<?php 
/**
*Template name:Партнеры
*/
?>
<?php get_header(); ?>
<div class="container-fluid">
	<div class="container bg_container">
		<div class="bg_partners">
<?php 
$child_categories = get_child_categories();//получение дочерних категорий
if (!empty($child_categories)) {
	foreach ($child_categories as $child_category) {
		$posts = give_posts($child_category->slug);//получение всех постов по дочерней категории
		if (!empty($posts)) { ?>
			<section class="<?=$child_category->slug ?>">
				<h2><?=$child_category->name ?></h2>
			<?php foreach( $posts as $post ){//вывод каждого поста клиентов ?>
				<div class="partner-item">
					<img src="<?php $img_url = get_field("appearance"); echo $img_url["url"]; ?>"></img>
					<h3 class="routine"><?php the_title(); ?></h3>
					<p class="routine"><?php echo $post->post_content; ?></p>
					<a class="routine" href="<?php echo "http://".get_field("partner_site"); ?>">Подробнее...</a>
				</div>
			<?php } ?>
			</section><?php echo "<!-- ".$child_category->slug."-->" ?>
		<?php }
	}
} else {
	echo "<h1 style=\"color: red; font-size: 25px; font-weight: bold;\">К сожелению, у Нас нет партнеров...</h1>";
} ?>
		</div><!-- /.bg_partners -->
	</div><!-- /.bg_container -->
</div><!-- /.bg_container-fluid -->
<div class="clearfix"></div>
<?php get_footer(); ?>