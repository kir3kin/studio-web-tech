<?php
get_header(); ?>
<div class="container-fluid">
	<div class="container bg_container">
		<div class="row clearfix">
			<?php get_sidebar(); ?>
			<div class="bg_blogs col-xs-12 col-sm-9">
<?php $request_date = $_SERVER['REQUEST_URI'];
if (is_day()) {
	$current_date = "day";
	$current_number = substr($request_date, -3, 2);
}
elseif (is_month()) {
	$current_date = "month";
	$current_number = substr($request_date, -3, 2);
}
elseif (is_year()) {
	$current_date = "year";
	$current_number = substr($request_date, -5, 4);
}
$posts = give_posts("", $current_date, $current_number);
if ($posts) {
	foreach( $posts as $post ){ the_post(); ?>
				<div class="bg_blog-item">
				<?php if (get_the_post_thumbnail()) { ?>
					<?php echo get_the_post_thumbnail(); ?>
				<?php } ?>
				<?php the_content(""); ?>
					<div class="more-link-wr">
						<a class="more-link" href="<?php echo home_url()."/".get_post()->post_name; ?>">Подробнее...</a>
					</div><!-- /.more-link-wr -->
				</div><!-- /.bg_blog-item -->
	<?php } 
} else { ?>
					<div class="bg_blog-item">
						<div class="not-found"><h2>По даному запросу ничего не найдено.</h2></div>
					</div><!-- /.bg_blog-item -->
<?php } ?>
			</div><!-- /.bg_blogs /.clearfix -->
		</div><!-- /.row -->
	</div><!-- /.bg_container -->
</div><!-- /.container-fluid -->
<?php get_footer(); ?>