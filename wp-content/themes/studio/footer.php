	<!-- footer start -->
		<footer class="footer" id="contact">
			<div class="container">
				<div class="footer__stuck-question wow slideInDown">
					<p class="footer__question mark">Остались вопросы?</p>
					<p class="footer__action">Задайте их менеджеру!</p>
				</div>
				<div class="footer__contact wow slideInDown">
					<p class="footer__tel">+38(096) 834-673-3</p>
					<p class="footer__tel">+38(095) 151-611-7</p>
					<p class="footer__skype">Skype: sergeykrnet</p>
					<p class="footer__email">Почта: sales@web-tehnology.com</p>
				</div>
			</div>
		</footer>
	<!-- footer end -->
	</div>
<!-- wrapper end -->
	<!--scripts-->
	<script src="<?php bloginfo('stylesheet_directory'); ?>/libs/jquery.min.js"></script>	
	<script src="<?php bloginfo('stylesheet_directory'); ?>/libs/jquery.maskedinput-1.2.2.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/libs/wow.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.min.js"></script>
	<script>
		function call(currentForm){
			var form_data = $(currentForm).serialize();
			 $.ajax({
				type: "POST",
				url: "<?php bloginfo('stylesheet_directory'); ?>/send/form.php",
				data: form_data,
				success: function () {
					$(".header__modal-success").fadeIn('400');
					setTimeout(function () {
						$(".header__modal-success").fadeOut('400');
						$(".header__modal").animate({opacity: 0}, 500);
						setTimeout(function(){
							$(".header__modal").addClass('header__modal_hide');
							$(".nav").removeAttr('style');
						},500);
					}, 3000);
					$("form").trigger("reset");  
				},
				error: function(xhr, str) {
					 $(".header__modal-error").fadeIn('400');
					 setTimeout(function () {
						$(".header__modal-error").fadeOut('400');
					}, 3000);
				 } 
			 });
		 }
		</script>
</body>
</html>