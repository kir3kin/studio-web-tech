<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="keywords" content="Создание сайтов, веб сайт, создать интернет-магазин, создать сайт">
	<meta name="description" content="ВЕБ студия WEB-tehnology. Cоздание сайта, создание интернет-магазина, Разработка WEB-приложений. Заказать сайт в Кривом Роге  ☎(096)834-67-33">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="HandheldFriendly" content="true">
	<meta name="yandex-verificatoin" content="6615d78911ec7902">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/media.css">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/fonts/elegant_font/HTML_CSS/style.css" type="text/css">
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/fonts/et-line-font/style.css" type="text/css">
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/libs/animate.css">
	<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.png" type="inage/x-icon">
	<title><?php bloginfo("name"); ?></title>
		<!-- [if lt IE 9] >
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<! [endif] -->
	</head>
<body>
<div id="page-preloader" class="cssload-container">
	<div class="cssload-speeding-wheel"></div>
</div>
<!-- wrapper start -->
	<div class="main">
		<!-- header start -->
		<nav class="nav animated slideInDown">
			<button class="navbar" onclick="changeBar(this)">
					<span class="bar1"></span>
					<span class="bar2"></span>
					<span class="bar3"></span>
			</button>
			<ul class="nav__list">
			<?php if (home_url($wp->request) === home_url()) { ?>
				<li class="nav__item"><a href="#main" class="nav_link">Главная</a></li>
				<li class="nav__item"><a href="#landing" class="nav_link">Создание сайта</a></li>
				<li class="nav__item"><a href="#portfolio" class="nav_link">Портфолио</a></li>
				<li class="nav__item"><a href="#web-programms" class="nav_link">WEB-Программы</a></li>
				<li class="nav__item"><a href="#steps" class="nav_link">Процесс роботы</a></li>
				<li class="nav__item"><a href="#contact" class="nav_link">Контакты</a></li>
			<?php } else { ?>
				<li class="nav__item"><a href="<?php bloginfo("url"); ?>/#main" class="nav_link">Главная</a></li>
				<li class="nav__item"><a href="<?php bloginfo("url"); ?>/#landing" class="nav_link">Создание сайта</a></li>
				<li class="nav__item"><a href="<?php bloginfo("url"); ?>/#portfolio" class="nav_link">Портфолио</a></li>
				<li class="nav__item"><a href="<?php bloginfo("url"); ?>/#web-programms" class="nav_link">WEB-Программы</a></li>
				<li class="nav__item"><a href="<?php bloginfo("url"); ?>/#steps" class="nav_link">Процесс роботы</a></li>
				<li class="nav__item"><a href="<?php bloginfo("url"); ?>/#contact" class="nav_link">Контакты</a></li>
			<?php } ?>
				<li class="nav__item blog_studio"><a href="<?php echo get_page_link("6"); ?>" class="nav_link">Блог</a></li>
				<li class="nav__item"><a href="<?php echo get_page_link("27"); ?>" class="nav_link">Партнёры</a></li>
			</ul>
		</nav>
		<header class="header" id="main">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo_prob.png" alt="web-technology logo" class="header__logo wow fadeIn" data-wow-delay="1s">
			<ul class="header__service-list wow fadeInDown" data-wow-delay="1s">
				<li class="header__service"><strong class="header__service-name">Сайты </strong></li>
				<li class="header__service"><strong class="header__service-name"><span>/</span>  WEB программы (CRM)  <span>/</span></strong></li>
				<li class="header__service"><strong class="header__service-name">Интернет-магазины</strong></li>
			</ul>
			<hgroup>
				<h1 class="header__main-header wow fadeInDown" data-wow-delay="1.2s">СОЗДАНИЕ САЙТОВ В КРИВОМ РОГЕ</h1>
				<h2 class="header__sub-header wow fadeInDown" data-wow-delay="1.4s">Используем новые и актуальные технологии в программировании</h2>
			</hgroup>
			<div class="header__btn_wrapper wow fadeInDown" data-wow-delay="1.6s">
				<button class="header__btn wow pulse" data-wow-delay="1.6s" data-wow-iteration="infinite"  data-wow-duration="2s">Узнай стоимость сайта</button>
			</div>
			<div class="header__modal header__modal_hide">
			<div class="header__modal-bg"></div>
			<div class="header__modal-success">Сообщение успешно отправлено</div>
			<div class="header__modal-error">Не отравлено</div>	
				<div class="header__modal-window">
				<div class="triangle"></div>
				<div class="close-window"></div>
					<h3 class="header__modal-window-header">Узнай стоимость <br> сайта</h3>
					<h4 class="header__modal-window-subheader">Пожалуйста, заполните поля</h4>
					<form action="javascript:void(null)" method="POST" class="header__modal-window-form" onsubmit="call(this)">
						<input type="text" name="username" class="header__modal-window-name" placeholder="Ваше имя" pattern="[А-Яа-я]і?{2,50}" maxlength="50" required>
						<input type="email" name="useremail" class="header__modal-window-mail" placeholder="Электронный адрес">
						<input type="tel" name="usertel" class="header__modal-window-tel" placeholder="Контактный телефон" required>
						<button type="submit" class="header__modal-window-send">Отправить</button>
					</form>
				</div>
			</div>
		</header>
		<!-- header end -->