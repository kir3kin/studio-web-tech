<!-- start sidebar -->
<div class="bg_sidebar col-xs-12 col-sm-3">

	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar') ) : ?>
	<div class="bg_sidebar-items">
	<?php $partner_cat_id = get_parent_cat_id();//определение id родительской категории для всех клиентов
	$args = array('title_li' => __(''), 'exclude' => $partner_cat_id);//вывод всех категорий кроме Клиенты ?>
		<h2>Категории</h2>
		<ul>
			<?php wp_list_categories($args); ?>
		</ul>
	</div><!-- /.bg_sidebar-items -->
	<?php endif; ?>

</div><!-- /.bg_sidebar -->
<!-- end sidebar -->