<?php
/**
* поддержка миниатюр для записей
**/
add_theme_support('post-thumbnails' );

define("CLIENTS_PARTNERTS", "clients_partnerts");//название родительской категории для клиентов

if (function_exists('register_sidebar')) {//фц-я регистрации сайдбара в вордпресс
  register_sidebar(
    array(
      'name'          => 'Сайдбар',
      'id'            => 'sidebar',
      'before_widget' => '<div class="bg_sidebar-items">',
      'after_widget'  => '</div><!-- /.bg_sidebar-items -->',
      'before_title'  => '<h2>',
      'after_title'   => '</h2>'
    )
  );
}

function give_posts($category = "", $current_date = "", $current_nubmer = "") {//фц-я для получения всех записей по категории
	$clients_id = exclude_clients($category);//id записей клиентов, если нужно вывести самих клиентов вернет ""
	$posts = get_posts( array(
		'posts_per_page'  => get_option("posts_per_page"),
		'offset'          => 0,
		'category_name'   => $category,//если значение пустое, то выбирет все категории
		'orderby'         => 'post_date',
		'order'           => '',
		'include'         => '',
		'exclude'         => $clients_id,//исключаем записи клиентов, если нужно
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'post',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'post_status'     => 'publish',
		$current_date     => $current_nubmer
	) );
	return $posts;
}

function exclude_clients($category) {//фц-я для получения записей клиентов
	$child_categories = get_child_categories();//получение дочерних категорий
	foreach ($child_categories as $child) {//если запросили клиентов, в исключения ничего не попадает
		if ($child->slug === $category) return "";
	}
	$category_ids = get_ids($child_categories, "term_id");//получение id дочерних категорий
	$posts_partners = get_posts( array(//получение id записей дочерних категорий
		'posts_per_page'  => -1,
		'category'        => $category_ids
	) );
	return get_ids($posts_partners, "ID");
}

function get_child_categories() {//фц-я получения дочерних категорий
	$parent_category = get_parent_cat_id();//определение id родительской категории для всех клиентов
	return get_categories(array('child_of' => $parent_category));
}

function get_ids($elms, $kind) {//фц-я для получения $kind полученых обьектов
	foreach ($elms as $elm) {
		$elms_ids .= $elm->$kind.",";
	}
	return substr($elms_ids, 0, -1);
}

function get_parent_cat_id() {//определение id родительской категории для всех клиентов
	return get_category_by_slug(CLIENTS_PARTNERTS)->term_id;
}
?>