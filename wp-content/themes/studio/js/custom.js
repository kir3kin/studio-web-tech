function changeBar(x) {
	x.classList.toggle("change");
}
new WOW().init();
$(document).ready(function(){
	$(".navbar").on("click",function () {
			$("nav ul").slideToggle(500);
	 });
	
	if($(window).width() <= 768){
		 $(".nav li a").on("click",function () {
				$("nav ul").slideUp(200);
				$(".navbar").removeClass("change");
		 });
	};   
	 //Плавный скролл по якорям
	$('a[href^="#"]').click(function(){
		elementClick = $(this).attr("href");
		destination = $(elementClick).offset().top;
			$('html, body').animate( {scrollTop: destination }, 1000 );
		return false;
	});
	//modal-header
	$(".header__btn,.order-to__btn,.footer__btn").click(function () {
		$(".header__modal").removeClass('header__modal_hide').animate({opacity: 1}, 500);
		$("nav").fadeOut('fast');
	});
	$(".header__modal-bg, .close-window").on('click', function() {
		$(".header__modal").animate({opacity: 0}, 400);
		setTimeout(function () {
			$(".header__modal").addClass('header__modal_hide');
		}, 400);
		$("nav").removeAttr('style');
	});
	$(".header__btn").on('mouseenter', function() {
		$(".header__btn").css('animation-iteration-count', '0');
	});
	$(".header__btn").on('mouseleave', function() {
		$(".header__btn").css('animation-iteration-count', 'infinite');
	});
	//
	$(window).on('load', function () {
			var $preloader = $('#page-preloader'),
					$spinner   = $preloader.find('.spinner');
			$spinner.fadeOut();
			$preloader.delay(350).fadeOut('slow');
	});
	//маска ввода

	jQuery(function($) {
		if ($(".more-link")) {//отображение описания постов в категориях и блоге
			$(".more-link").parent().find(".description").css("display", "block");
		}

		if (location.href.slice(0, -1) !== location.origin) {//уменьшение размера основной картинки в категориях и блоге
			if (location.href.slice(0, -1).indexOf("#") === -1) {
				$(".header").css("min-height", "65vh");
				$(".header__logo").css("margin", "80px 0 20px");
			}
		}

		// ::::::::::::::::::::::::::::::::: //
		// :::::: Размещения статьей ::::::: //
		// ::::::::::::::::::::::::::::::::: //
		var blogMaker = function() {//фц-я распределения и размещения статьей в 2 колонки
			var displayWidth = $(window).width();//текущая ширина экрана
			var fposth = $('.bg_blog-item:first-of-type').outerHeight(true);//по умолчанию полная высота первой статьи, а после первой колонки статей
			var fposth2 = fposth;//по умолчанию полная высота второй статьи, а после второй колонки статей
			var articlecount = 0;
			var firstcolw = "52%";//ширина первой колонки статей с отступом в 4%
			var posBlogArticle = function(objElm, postElm) {//передаем текущие обьект статьи и высоту колонки
				objElm.css('top', postElm);
				return postElm += objElm.outerHeight(true);//возвращает дополненное значение высоты текущей колонки
			};
			var posBlogArticleExtended = function(objElm, postElm, firstCol) {//передаем текущие обьект статьи, высоту колонки и ширину второй колонки с отступом
				objElm.css('left', firstCol);
				return posBlogArticle(objElm, postElm);
			};

			if (displayWidth <= 992) {//чувствительный к ширине экрана
				fposth = firstcolw = 0;
				$('.bg_blog-item').each(function(){
					fposth = posBlogArticleExtended($(this), fposth, firstcolw);
				});
				$('.bg_blogs').height(fposth);//установление высоты блога со статьями
			}
			else {
				$('.bg_blog-item').each(function(){
					if (articlecount != 0){
						if (fposth <= fposth2) {
							fposth = posBlogArticle($(this), fposth);
						}
						else {
							fposth2 = posBlogArticleExtended($(this), fposth2, firstcolw);
						}
						articlecount++
					} else articlecount++;
				});
				if (fposth >= fposth2) $('.bg_blogs').height(fposth);//установление высоты блога со статьями
				else $('.bg_blogs').height(fposth2);
			}
		};
		setTimeout(blogMaker, 40);
		$(window).resize(function() { blogMaker(); });//динамическое размещения статьей


			$.mask.definitions['~']='[+-]';
			$('.header__modal-window-tel').mask('(099) 999-99-99');
			$('#date').mask('99/99/9999');
			$('#phone').mask('(999) 999-9999');
			$('#phoneext').mask("(999) 999-9999? x99999");
			$("#tin").mask("99-9999999");
			$("#ssn").mask("999-99-9999");
			$("#product").mask("a*-999-a999");
			$("#eyescript").mask("~9.99 ~9.99 999");
	});
});
