<?php get_header(); ?>
<div class="container-fluid">
	<div class="container bg_container">
		<div class="row clearfix">
			<?php get_sidebar(); ?>
			<div class="bg_blogs col-xs-12 col-sm-9">
				<div class="bg_blog-item">
					<?php if (have_posts()) { the_post();
						the_content("", true); } ?>
				</div>
			</div><!-- /.bg_blogs /.clearfix -->
		</div><!-- /.row -->
	</div><!-- /.bg_container -->
</div><!-- /.container-fluid -->
<?php get_footer(); ?>