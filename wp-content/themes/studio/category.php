<?php
/**
*Template name:Блог и категории
*/
?>
<?php get_header(); ?>
<div class="container-fluid">
	<div class="container bg_container">
		<div class="row clearfix">
			<?php get_sidebar(); ?>
			<div class="bg_blogs col-xs-12 col-sm-9">
<?php $cat_name = $GLOBALS["category_name"];
$posts = give_posts($cat_name);
foreach( $posts as $post ){ the_post(); ?>
				<div class="bg_blog-item">
				<?php if (get_the_post_thumbnail()) { ?>
					<?php echo get_the_post_thumbnail(); ?>
				<?php } ?>
				<?php the_content(""); ?>
					<div class="more-link-wr">
						<a class="more-link" href="<?php echo home_url()."/".get_post()->post_name; ?>">Подробнее...</a>
					</div><!-- /.more-link-wr -->
				</div><!-- /.bg_blog-item -->
<?php } ?>
			</div><!-- /.bg_blogs /.clearfix -->
		</div><!-- /.row -->
	</div><!-- /.bg_container -->
</div><!-- /.container-fluid -->
<?php get_footer(); ?>