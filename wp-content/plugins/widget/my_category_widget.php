<?php
/*
Plugin Name: Show Category
Plugin URI: http://crm-system.pp.ua
Author: Igor Ahabanin
Description: Добавляет виджет, для вывода Рубирик блога, за исключением рубрики "Клиенты и партнёры."
 */
class widget_my_category extends WP_Widget {

	public function __construct() {
		parent::__construct(
			'my_category',
			'Рубрики (без клиентов и партнёров)',
			array('description' => 'Выводит все категории, кроме рубрики "Клиенты и партнёры"')
		);
	}

	public function widget( $args, $instance ) {
		$partner_cat_id = get_parent_cat_id();//определение id родительской категории для всех клиентов
		$arguments = array('title_li' => __(''), 'exclude' => $partner_cat_id);//для вывода всех категорий кроме Клиенты
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $args['before_widget'];
		echo $args['before_title'] . $title . $args['after_title']; ?>
		<ul>
		<?php
		wp_list_categories( $arguments );
		?>
		</ul>
		<?php
		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = (!empty($new_instance))? $new_instance['title']: '';
		return $instance;
	}
	
	public function form( $instance ) {
		$title = sanitize_text_field( $instance['title'] );
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php
	}
}

function my_wp_load_widget() {
	register_widget("widget_my_category");
}

add_action("widgets_init", "my_wp_load_widget");

?>